// [[1,2,[3]],4] => [1,2,3,4]

module.exports = function arrFlatten(input) {

  // minor optimization: we don't want to evaluate `input.length` every loop
  let len = input.length;

  // the resulting `output` array (initially an empty array)
  let output = [];

  // loop through the input array's children
  for (let i = 0; i < len; i++) {
    const target = input[i];
    if (Array.isArray(target)) {
      // if the child is an array, recursively perform `arrFlatten` on it,
      // then push the flattened array's elements into the resulting `output` array
      output.push(...arrFlatten(target));
    } else {
      // if the child is NOT an array, directly add it into the resulting
      // `output` array
      output.push(target);
    }
  }
  return output;
}
