const arrayFlatten = require("./array-flatten");
const assert = require("assert");

describe("array-flatten", () => {

  const cases = [

    [[1,2,[3]],4],
    [1, 2, 3, 4],

    [5, [6, [7, 8, [[9]]]]],
    [5, 6, 7, 8, 9],

    [undefined, null, [Infinity, [NaN]], -Infinity, 10, 11, [12, false, [[{"foo": "bar"}]]]],
    [undefined, null, Infinity, NaN, -Infinity, 10, 11, 12, false, {"foo": "bar"}],

    [],
    [],

    [[[[13, 14, [15, 16]]]]],
    [13, 14, 15, 16]

  ];

  for (let i = 0; i < cases.length - 1; i += 2) {
    it(`should pass test case ${i/2 + 1}`, () => {

      const input = cases[i];
      const output = cases[i + 1];

      const res = arrayFlatten(input);
      const val1 = res.join(",");
      const val2 = output.join(",");

      assert(val1 === val2, val1 + " does not match " + val2);

    });
  }

});
